package com.example.moviedatabase.movie.presentation

import android.view.View
import com.airbnb.epoxy.EpoxyController
import com.example.moviedatabase.movie.presentation.MovieItemModel_
import com.example.moviedatabase.movie.domain.model.Movie

class MovieController (private val listener: (View, String) -> Unit) : EpoxyController() {

    var listOfMovies = mutableListOf<Movie>()

    override fun buildModels() {
        listOfMovies?.let {
            it.forEachIndexed { index, movie ->
                MovieItemModel_()
                        .id(index)
                    ._id(movie._id)
                    .title(movie.title + " (" + movie.year + ")")
                    .poster(movie.poster)
                    .directors("by " + movie.directors.toString().replace("[", "").replace("]", ""))
                        .genres(movie.genres.toString().replace("[", "").replace("]", ""))
                    .clickListener { model, _, view, _ ->
                        listener.invoke(view, model._id())
                    }
                    .addTo(this)
            }
        }
    }


    fun setData (movies : List<Movie>) {
        for (movie in movies) {
            listOfMovies.add(movie)
        }
        requestModelBuild()
    }
}