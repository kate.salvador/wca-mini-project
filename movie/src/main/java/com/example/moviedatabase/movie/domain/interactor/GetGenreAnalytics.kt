package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository

class GetGenreAnalytics(private val repository: MovieRepository) {

    suspend operator fun invoke() : List<List<Any>> {
        return repository.getGenreAnalytics()
    }
}