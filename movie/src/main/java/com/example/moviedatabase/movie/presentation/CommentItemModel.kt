package com.example.moviedatabase.movie.presentation

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.moviedatabase.common.BaseEpoxyHolder
import com.example.moviedatabase.movie.R
import kotlinx.android.synthetic.main.item_comment.view.*

@EpoxyModelClass
abstract class CommentItemModel : EpoxyModelWithHolder<CommentItemModel.ViewHolder>() {

    override fun getDefaultLayout(): Int {
        return R.layout.item_comment
    }

    @EpoxyAttribute
    lateinit var name : String

    @EpoxyAttribute
    lateinit var text : String

    @EpoxyAttribute
    lateinit var date : String

    class ViewHolder : BaseEpoxyHolder() {
        lateinit var textName : TextView
        lateinit var textComment : TextView
        lateinit var textDate : TextView

        override fun bindView(itemView: View) {
            super.bindView(itemView)
            itemView.let {
                textName = it.tv_name
                textComment = it.tv_comment
                textDate = it.tv_date
            }
        }
    }

    override fun bind(holder: ViewHolder) {
        super.bind(holder)
        holder.textName.text = name
        holder.textComment.text = text
        holder.textDate.text = date

    }
}