package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable


@Serializable
data class Comment (
    val movie_id : String,
    val _id : String,
    val name : String,
    val email : String,
    val text : String,
    val date : String
)