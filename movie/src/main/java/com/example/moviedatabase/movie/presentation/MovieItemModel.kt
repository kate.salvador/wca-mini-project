package com.example.moviedatabase.movie.presentation

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.moviedatabase.common.BaseEpoxyHolder
import com.example.moviedatabase.movie.R
import com.google.android.material.imageview.ShapeableImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

@EpoxyModelClass
abstract class MovieItemModel : EpoxyModelWithHolder<MovieItemModel.ViewHolder>() {


    override fun getDefaultLayout(): Int {
        return R.layout.item_movie
    }

    @EpoxyAttribute
    lateinit var _id : String

    @EpoxyAttribute
    lateinit var title : String

    @EpoxyAttribute
    lateinit var directors : String

    @EpoxyAttribute
    lateinit var poster : String

    @EpoxyAttribute
    lateinit var genres : String

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    class ViewHolder : BaseEpoxyHolder() {
        lateinit var textTitle : TextView
        lateinit var textDirectores : TextView
        lateinit var cardMovie : View
        lateinit var imageMovie : ShapeableImageView
        lateinit var textGenres : TextView

        override fun bindView(itemView: View) {
            super.bindView(itemView)
            itemView.let {
                textTitle = it.tv_movie_title
                textDirectores = it.tv_movie_directors
                cardMovie = it.crd_movie
                imageMovie = it.img_movie
                textGenres = it.tv_movie_genres
            }
        }
    }

    override fun unbind(holder: ViewHolder) {
        super.unbind(holder)
        holder.cardMovie.setOnClickListener(null)
    }

    override fun bind(holder: ViewHolder) {
        super.bind(holder)
        Picasso.get().load(poster).into(holder.imageMovie)
        holder.textTitle.text = title
        holder.textDirectores.text = directors
        holder.cardMovie.setOnClickListener(clickListener)
        holder.textGenres.text = genres
    }
}