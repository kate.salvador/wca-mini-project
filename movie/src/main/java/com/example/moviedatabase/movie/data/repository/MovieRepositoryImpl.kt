package com.example.moviedatabase.movie.data.repository

import com.example.moviedatabase.movie.domain.repository.MovieRepository
import com.example.moviedatabase.movie.data.remote.MovieRemoteSource
import com.example.moviedatabase.movie.domain.model.Comment
import com.example.moviedatabase.movie.domain.model.Movie
import com.example.moviedatabase.movie.domain.model.Post

class MovieRepositoryImpl (private val remote: MovieRemoteSource) : MovieRepository {

    override suspend fun getMovies(page : String) : Post {
        return remote.getMovies(page)
    }

    override suspend fun getMovieById(id : String) : Movie {
        return remote.getMovieById(id)
    }

    override suspend fun getComments(id: String): List<Comment> {
        return remote.getComments(id)
    }

    override suspend fun searchMovies(page: String, search: String): Post {
        return remote.searchMovies(page, search)
    }

    override suspend fun getYearAnalytics(): List<List<Int>> {
        return remote.getYearAnalytics()
    }

    override suspend fun getGenreAnalytics(): List<List<Any>> {
        return remote.getGenreAnalytics()
    }

    override suspend fun getAuth(token : String): String {
        return remote.getAuth(token)
    }
}