package com.example.moviedatabase.movie.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.moviedatabase.movie.R
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import kotlinx.android.synthetic.main.fragment_movie_analytics.*

class MovieAnalyticsFragment : Fragment() {
    private val viewModelFactory : MovieViewModelFactory by lazy { MovieViewModelFactory() }
    private val viewModel : MovieViewModel by viewModels() { viewModelFactory }

    var listOfYearCoordinates = arrayListOf<BarEntry>()
    var listOfYears = arrayListOf<String>()
    lateinit var yearDataSet: BarDataSet
    lateinit var yearData: BarData

    var listOfGenreCoordinates = arrayListOf<BarEntry>()
    var listOfGenres = arrayListOf<String>()
    lateinit var genreDataSet: BarDataSet
    lateinit var genreData: BarData


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            viewModel.loadYearAnalytics()
            viewModel.yearAnalytics.observe(viewLifecycleOwner){
                for(i in 0..it.size-1){
                    listOfYearCoordinates.add(BarEntry(i.toFloat(), it[i][1].toFloat()))
                    listOfYears.add(it[i][0].toString())
                }

                yearDataSet = BarDataSet(listOfYearCoordinates, "Movies")
                yearData = BarData(yearDataSet)


                year_chart.setData(yearData)
                year_chart.getDescription().setText("Year")
                year_chart.getXAxis().setValueFormatter(IndexAxisValueFormatter(listOfYears))
                year_chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE)
                year_chart.getXAxis().setDrawGridLines(false)
                year_chart.getXAxis().setDrawAxisLine(false)
                year_chart.getXAxis().setGranularity(1f)
                year_chart.getXAxis().setLabelCount(listOfYears.size)
                year_chart.getXAxis().setLabelRotationAngle(270F)
                year_chart.animateY(2000)
                year_chart.invalidate()
            }
        } catch (e: Exception) {
            Toast.makeText(
                    requireActivity(),
                    "Exception Occurred: ${e.message}",
                    Toast.LENGTH_SHORT
            ).show()
        }

//        try {
//            viewModel.loadGenreAnalytics()
//            viewModel.genreAnalytics.observe(viewLifecycleOwner){
//                for(i in 0..it.size-1){
//                    listOfGenreCoordinates.add(BarEntry(i.toFloat(), it[i][1].toFloat()))
//                    listOfGenres.add(it[i][0].toString())
//                }
//
//                genreDataSet = BarDataSet(listOfGenreCoordinates, "Movies")
//                genreData = BarData(genreDataSet)
//
//
//                genre_chart.setData(genreData)
//                genre_chart.getDescription().setText("Genre")
//                genre_chart.getXAxis().setValueFormatter(IndexAxisValueFormatter(listOfGenres))
//                genre_chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE)
//                genre_chart.getXAxis().setDrawGridLines(false)
//                genre_chart.getXAxis().setDrawAxisLine(false)
//                genre_chart.getXAxis().setGranularity(1f)
//                genre_chart.getXAxis().setLabelCount(listOfGenres.size)
//                genre_chart.getXAxis().setLabelRotationAngle(270F)
//                genre_chart.animateY(2000)
//                genre_chart.invalidate()
//            }
//        } catch (e: Exception) {
//            Toast.makeText(
//                    requireActivity(),
//                    "Exception Occurred: ${e.message}",
//                    Toast.LENGTH_SHORT
//            ).show()
//        }



    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_movie_analytics, container, false)
    }
}