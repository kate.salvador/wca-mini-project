package com.example.moviedatabase.movie.data.remote

import com.example.moviedatabase.movie.domain.model.Comment
import com.example.moviedatabase.movie.domain.model.Movie
import com.example.moviedatabase.movie.domain.model.Post
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("/movies")
    suspend fun getMovies(
        @Query("page") page : String
    ): Response<Post>

    @GET("/movies/{id}")
    suspend fun getMovieById(
        @Path("id") id : String
    ) : Response<Movie>

    @GET("/movies/{id}/comments")
    suspend fun getComments(
        @Path("id") id : String
    ) : Response<List<Comment>>

    @GET("/search")
    suspend fun searchMovies(
        @Query ("page") page : String,
        @Query ("search") search : String
    ) : Response <Post>

    @GET("/analytics/year")
    suspend fun getYearAnalytics() : Response<List<List<Int>>>

    @GET("/analytics/genre")
    suspend fun getGenreAnalytics() : Response<List<List<Any>>>

    @GET("movies/filtered/test")
    suspend fun getAuth(
            @Header("Token") token : String
    ) : Response<String>
}