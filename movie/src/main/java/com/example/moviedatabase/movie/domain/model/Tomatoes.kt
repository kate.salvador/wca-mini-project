package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Tomatoes (

    val viewer : Viewer,
    val lastUpdated : String
)