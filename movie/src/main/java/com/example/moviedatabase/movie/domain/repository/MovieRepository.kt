package com.example.moviedatabase.movie.domain.repository

import androidx.annotation.Nullable
import com.example.moviedatabase.movie.domain.model.Comment
import com.example.moviedatabase.movie.domain.model.Movie
import com.example.moviedatabase.movie.domain.model.Post

interface MovieRepository {

    @Nullable
    suspend fun getMovies(page : String) : Post
    suspend fun getMovieById(id : String) : Movie
    suspend fun getComments(id : String) : List<Comment>
    suspend fun searchMovies(page : String, search : String) : Post
    suspend fun getYearAnalytics() : List<List<Int>>
    suspend fun getGenreAnalytics() : List<List<Any>>
    suspend fun getAuth(token : String) : String
}