package com.example.moviedatabase.movie.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviedatabase.movie.MovieServiceLocator
import com.example.moviedatabase.movie.domain.interactor.*
import com.example.moviedatabase.movie.domain.model.Comment
import com.example.moviedatabase.movie.domain.model.Movie
import kotlinx.coroutines.launch

class MovieViewModel (
    val getMovies: GetMovies,
    val getMovieById: GetMovieById,
    val getComments: GetComments,
    val searchMovies: SearchMovies,
    val getYearAnalytics: GetYearAnalytics,
    val getGenreAnalytics: GetGenreAnalytics,
    val getAuth: GetAuth
) : ViewModel() {

    private val _movies = MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>>
        get() = _movies

    private val _movie = MutableLiveData<Movie>()
    val movie: LiveData<Movie>
        get() = _movie

    private val _comments = MutableLiveData<List<Comment>>()
    val comments: LiveData<List<Comment>>
        get() = _comments

    private val _searchResults = MutableLiveData<List<Movie>>()
    val searchResults: LiveData<List<Movie>>
        get() = _searchResults

    private val _yearAnalytics = MutableLiveData<List<List<Int>>>()
    val yearAnalytics: LiveData<List<List<Int>>>
        get() = _yearAnalytics

    private val _genreAnalytics = MutableLiveData<List<List<Any>>>()
    val genreAnalytics: LiveData<List<List<Any>>>
        get() = _genreAnalytics

    private val _token = MutableLiveData<String>()
    val token : LiveData<String>
        get() = _token

    private val _homeCurrentPage = MutableLiveData<String>()
    val homeCurrentPage : LiveData<String>
        get() = _homeCurrentPage

    private val _searchCurrentPage = MutableLiveData<String>()
    val searchCurrentPage : LiveData<String>
        get() = _searchCurrentPage

    fun loadMovies(page : String){
        viewModelScope.launch {
            _movies.value = getMovies(page).posts
        }
    }

    fun loadHomeCurrentPage(page : String){
        viewModelScope.launch {
            _homeCurrentPage.value = getMovies(page).currentPage
        }
    }

    fun loadMovieById(id : String) {
        viewModelScope.launch {
            _movie.value = getMovieById(id)
        }
    }

    fun loadComments(id: String) {
        viewModelScope.launch {
            _comments.value = getComments(id)
        }
    }

    fun loadSearch(page : String, search : String) {
        viewModelScope.launch {
            _searchResults.value = searchMovies(page, search).posts
        }
    }

    fun loadSearchCurrentPage(page : String, search : String){
        viewModelScope.launch {
            _searchCurrentPage.value = searchMovies(page, search).currentPage
        }
    }

    fun loadYearAnalytics() {
        viewModelScope.launch {
            _yearAnalytics.value = getYearAnalytics()
        }
    }

    fun loadGenreAnalytics() {
        viewModelScope.launch {
            _genreAnalytics.value = getGenreAnalytics()
        }
    }

    fun loadAuth(token : String) {
        viewModelScope.launch {
            _token.value = getAuth(token)
        }
    }

    companion object {
        fun create() = MovieViewModel(
                MovieServiceLocator.getMovies,
                MovieServiceLocator.getMovieById,
                MovieServiceLocator.getComments,
                MovieServiceLocator.searchMovies,
                MovieServiceLocator.getYearAnalytics,
                MovieServiceLocator.getGenreAnalytics,
                MovieServiceLocator.getAuth
        )
    }
}