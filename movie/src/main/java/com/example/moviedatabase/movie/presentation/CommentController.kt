package com.example.moviedatabase.movie.presentation

import com.airbnb.epoxy.EpoxyController
import com.example.moviedatabase.movie.presentation.CommentItemModel_
import com.example.moviedatabase.movie.domain.model.Comment

class CommentController: EpoxyController() {

    var listOfComments = mutableListOf<Comment>()

    override fun buildModels() {
        listOfComments?.let {
            it.forEachIndexed { index, comment ->
                CommentItemModel_()
                    .id(index)
                    .name(comment.name)
                    .text(comment.text)
                    .date(comment.date)
                    .addTo(this)
            }
        }
    }

    fun setData (comments : List<Comment>) {
        for (comment in comments) {
            listOfComments.add(comment)
        }
        requestModelBuild()
    }
}