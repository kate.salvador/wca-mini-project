package com.example.moviedatabase.movie.domain.interactor

import androidx.annotation.Nullable
import com.example.moviedatabase.movie.domain.repository.MovieRepository
import com.example.moviedatabase.movie.domain.model.Post

class GetMovies (private val repository: MovieRepository) {

    @Nullable
    suspend operator fun invoke(page : String) : Post {
        return repository.getMovies(page)
    }
}