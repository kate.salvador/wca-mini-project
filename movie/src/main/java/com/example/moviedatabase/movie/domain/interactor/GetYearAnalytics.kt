package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository

class GetYearAnalytics(private val repository: MovieRepository) {

    suspend operator fun invoke() : List<List<Int>> {
        return repository.getYearAnalytics()
    }
}