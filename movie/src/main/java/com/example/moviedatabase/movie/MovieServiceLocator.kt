package com.example.moviedatabase.movie

import com.example.moviedatabase.common.CommonServiceLocator
import com.example.moviedatabase.movie.data.remote.MovieRemoteSource
import com.example.moviedatabase.movie.data.remote.MovieService
import com.example.moviedatabase.movie.data.repository.MovieRepositoryImpl
import com.example.moviedatabase.movie.domain.interactor.*
import retrofit2.create

object MovieServiceLocator {

    private val movieService : MovieService = CommonServiceLocator.service.create()
    private val movieRemoteSource = MovieRemoteSource(movieService)
    private val movieRepository = MovieRepositoryImpl(movieRemoteSource)

    val getMovies = GetMovies(movieRepository)
    val getMovieById = GetMovieById(movieRepository)
    val getComments = GetComments(movieRepository)
    val searchMovies = SearchMovies(movieRepository)
    val getYearAnalytics = GetYearAnalytics(movieRepository)
    val getGenreAnalytics = GetGenreAnalytics(movieRepository)
    val getAuth = GetAuth(movieRepository)
}