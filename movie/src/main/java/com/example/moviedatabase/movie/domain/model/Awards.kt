package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Awards (

    val wins : Int,
    val nominations : Int,
    val text : String
)