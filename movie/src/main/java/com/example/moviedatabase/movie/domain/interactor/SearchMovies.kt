package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository
import com.example.moviedatabase.movie.domain.model.Post

class SearchMovies (private val repository: MovieRepository) {

    suspend operator fun invoke(page : String, search : String) : Post {
        return repository.searchMovies(page, search)
    }
}