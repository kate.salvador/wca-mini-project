package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository
import com.example.moviedatabase.movie.domain.model.Comment

class GetComments (private val repository: MovieRepository) {

    suspend operator fun invoke(id : String) : List<Comment> {
        return repository.getComments(id)
    }
}