package com.example.moviedatabase.movie.presentation

import android.content.Intent
import android.widget.Toast
import androidx.activity.viewModels

import com.example.moviedatabase.common.BaseActivity
import com.example.moviedatabase.movie.R
import kotlinx.android.synthetic.main.activity_movie.*

class MovieActivity : BaseActivity(R.layout.activity_movie) {

    private val viewModelFactory : MovieViewModelFactory by lazy { MovieViewModelFactory() }
    private val viewModel : MovieViewModel by viewModels() { viewModelFactory }

    override fun start() {
        supportActionBar?.hide()

        btn_logout.setOnClickListener {
            logout()
            Toast.makeText(this, "Successfully logged out!", Toast.LENGTH_LONG).show()
        }

        //Obtain the token from the Intent's extras
        val accessToken = intent.getStringExtra("EXTRA_ACCESS_TOKEN").toString()

        Toast.makeText(
                this@MovieActivity,
                "Access Token: $accessToken",
                Toast.LENGTH_SHORT
        ).show()

        //authorization
//        try {
//                viewModel.loadAuth(accessToken)
//                viewModel.token.observe(this){
//                    Toast.makeText(
//                            this,
//                            it.toString(),
//                            Toast.LENGTH_SHORT
//                    ).show()
//                }
//            } catch (e: Exception) {
//                Toast.makeText(
//                        this,
//                        "Exception Occurred: ${e.message}",
//                        Toast.LENGTH_SHORT
//                ).show()
//            }


    }


    private fun logout() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("EXTRA_CLEAR_CREDENTIALS", true)
        startActivity(intent)
        finish()
    }


}