package com.example.moviedatabase.movie.presentation

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.annotation.NonNull
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.example.moviedatabase.movie.R
import java.lang.String

class LoginActivity : AppCompatActivity() {

    private var auth0: Auth0? = null

    val EXTRA_CLEAR_CREDENTIALS = "com.auth0.CLEAR_CREDENTIALS"
    val EXTRA_ACCESS_TOKEN = "com.auth0.ACCESS_TOKEN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.hide()

        findViewById<Button>(R.id.btn_login).setOnClickListener {
            login()
        }

        auth0 = Auth0(this)
        auth0!!.setOIDCConformant(true)

        if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logout()
        }
    }

    private fun login() {
        WebAuthProvider.login(auth0!!)
                .withScheme("demo")
                .withAudience(
                        String.format(
                                "https://%s/userinfo",
                                getString(R.string.com_auth0_domain)
                        )
                )
                .start(this, object : AuthCallback {
                    override fun onFailure(@NonNull dialog: Dialog) {
                        runOnUiThread { dialog.show() }
                    }

                    override fun onFailure(exception: AuthenticationException) {
                        runOnUiThread {
                            Toast.makeText(
                                    this@LoginActivity,
                                    "Error: " + exception.message,
                                    Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onSuccess(@NonNull credentials: Credentials) {
                        runOnUiThread {

                            val intent = Intent(this@LoginActivity, MovieActivity::class.java)
                            intent.putExtra("EXTRA_ACCESS_TOKEN", credentials.accessToken.toString())
                            startActivity(intent)
                            finish()
                        }
                    }
                })
    }

    private fun logout() {
        WebAuthProvider.logout(auth0!!)
                .withScheme("demo")
                .start(this, object : VoidCallback {
                    override fun onSuccess(payload: Void?) {}
                    override fun onFailure(error: Auth0Exception) {
                        //Log out canceled, keep the user logged in
                        showNextActivity()
                    }
                })
    }

    private fun showNextActivity() {
        val intent = Intent(this@LoginActivity, MovieActivity::class.java)
        startActivity(intent)
        finish()
    }
}