package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Post (

    val posts : List<Movie>,
    val totalPages : Int,
    val currentPage : String
)