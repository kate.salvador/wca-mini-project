package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository
import com.example.moviedatabase.movie.domain.model.Movie

class GetMovieById(private val repository: MovieRepository) {

    suspend operator fun invoke(id : String) : Movie {
        return repository.getMovieById(id)
    }
}