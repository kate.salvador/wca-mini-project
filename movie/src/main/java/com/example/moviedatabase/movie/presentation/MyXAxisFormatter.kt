package com.example.moviedatabase.movie.presentation

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter

class MyXAxisFormatter(val listOfYears : List<Int>) : ValueFormatter() {

    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
        if (axis != null) {
            axis.setLabelCount(listOfYears.size,true)
        }
        return listOfYears.toString()
    }
}