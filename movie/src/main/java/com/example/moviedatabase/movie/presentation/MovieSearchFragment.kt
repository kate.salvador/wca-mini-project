package com.example.moviedatabase.movie.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviedatabase.movie.presentation.MovieSearchFragmentDirections
import com.example.moviedatabase.movie.R
import kotlinx.android.synthetic.main.fragment_movie_home.movie_recycler_view
import kotlinx.android.synthetic.main.fragment_movie_search.*

class MovieSearchFragment : Fragment() {
    lateinit var movieController : MovieController
    private val viewModelFactory : MovieViewModelFactory by lazy { MovieViewModelFactory() }
    private val viewModel : MovieViewModel by viewModels() { viewModelFactory }

    var currentPage = 1

    var prevPage = currentPage-1
    var nextPage = currentPage+1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadSearch(currentPage.toString(), edt_search.text.toString())

        btn_search.setOnClickListener{
            loadSearch("1", edt_search.text.toString())
        }

        btn_search_prev.setOnClickListener {
            try {
                viewModel.loadSearchCurrentPage(prevPage.toString(), edt_search.text.toString())
                viewModel.searchCurrentPage.observe(viewLifecycleOwner){
                    currentPage = it.toInt()
                    Toast.makeText(
                            this.requireActivity(),
                            currentPage.toString(),
                            Toast.LENGTH_SHORT
                    ).show()
                    loadSearch(it, edt_search.text.toString())
                }
            } catch (e: Exception) {
                Toast.makeText(
                        this.requireActivity(),
                        "Exception Occurred: ${e.message}",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

        btn_search_next.setOnClickListener {
            try {
                viewModel.loadSearchCurrentPage(nextPage.toString(), edt_search.text.toString())
                viewModel.searchCurrentPage.observe(viewLifecycleOwner){
                    currentPage = it.toInt()
                    Toast.makeText(
                            this.requireActivity(),
                            currentPage.toString(),
                            Toast.LENGTH_SHORT
                    ).show()
                    loadSearch(it, edt_search.text.toString())
                }
            } catch (e: Exception) {
                Toast.makeText(
                        this.requireActivity(),
                        "Exception Occurred: ${e.message}",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }



    fun loadSearch(page : String, search : String) {
        initController()
        try {
            viewModel.loadSearch(page, search)
            viewModel.searchResults.observe(viewLifecycleOwner){
                movieController.setData(it)
            }
        } catch (e: Exception) {
            Toast.makeText(
                this.requireActivity(),
                "Exception Occurred: ${e.message}",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun initController() {

        val clickListener: (View, String) -> Unit = { view, id ->
            viewModel.loadMovieById(id)
            viewModel.movie.observe(viewLifecycleOwner) {
                val poster = it.poster
                val title = it.title
                val directors = it.directors
                val writers = it.writers
                val genres = it.genres
                val rated = it.rated
                val year = it.year
                val fullplot = it.fullplot
                val cast = it.cast
                val awards = it.awards
                val imdb = it.imdb.rating
                val countries = it.countries
                val _id = it._id

                val action = MovieSearchFragmentDirections.actionMovieSearchFragmentToMovieDetailsFragment(
                        poster,
                        title,
                        directors.toString(),
                        writers.toString(),
                        genres.toString(),
                        rated,
                        year,
                        fullplot,
                        cast.toString(),
                        awards.text,
                        imdb.toString(),
                        countries.toString(),
                        _id,
                )
                findNavController().navigate(action)
            }
        }

        movieController = MovieController(clickListener)
//      movie_recycler_view.layoutManager = LinearLayoutManager(this.requireActivity())
        movie_recycler_view.layoutManager = GridLayoutManager(this.requireActivity(), 2)
        movie_recycler_view.setControllerAndBuildModels(movieController)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_search, container, false)
    }
}