package com.example.moviedatabase.movie.data.remote

import com.example.moviedatabase.movie.domain.model.Comment
import com.example.moviedatabase.movie.domain.model.Movie
import com.example.moviedatabase.movie.domain.model.Post

class MovieRemoteSource(private val service: MovieService) {

    suspend fun getMovies(page : String) : Post {
        val response = service.getMovies(page)
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun getMovieById(id : String) : Movie {
        val response = service.getMovieById(id)
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun getComments(id : String) : List<Comment> {
        val response = service.getComments(id)
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun searchMovies(page : String, search : String) : Post {
        val response = service.searchMovies(page, search)
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun getYearAnalytics() : List<List<Int>> {
        val response = service.getYearAnalytics()
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun getGenreAnalytics() : List<List<Any>> {
        val response = service.getGenreAnalytics()
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

    suspend fun getAuth(token : String) : String {
        val response = service.getAuth(token)
        val data = response.body()

        if (response.isSuccessful && data!=null) {
            return data
        } else {
            throw Exception(response.code().toString())
        }
    }

}