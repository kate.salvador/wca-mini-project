package com.example.moviedatabase.movie.presentation

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedatabase.movie.presentation.MovieDetailsFragmentArgs
import com.example.moviedatabase.movie.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_details.*


class MovieDetailsFragment : Fragment() {
    lateinit var commentController: CommentController
    private val viewModelFactory : MovieViewModelFactory by lazy { MovieViewModelFactory() }
    private val viewModel : MovieViewModel by viewModels() { viewModelFactory }

    val args : MovieDetailsFragmentArgs by navArgs()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        commentController = CommentController()
        comment_recycler_view.layoutManager = LinearLayoutManager(this.requireActivity())
        comment_recycler_view.setControllerAndBuildModels(commentController)

        try {
            viewModel.loadComments(args.Id)
            viewModel.comments.observe(viewLifecycleOwner) {
                commentController.setData(it)
            }
        } catch (e: Exception) {
            Toast.makeText(
                this.requireActivity(),
                "Exception Occurred: ${e.message}",
                Toast.LENGTH_SHORT
            ).show()
        }

        Picasso.get().load(args.poster).into(view.findViewById<ImageView>(R.id.imgv_details_poster));

        view.findViewById<TextView>(R.id.tv_details_title).text = args.title
        view.findViewById<TextView>(R.id.tv_details_directors).text = "Directed by: "+ args.directors.replace("[", "").replace("]", "")
        view.findViewById<TextView>(R.id.tv_details_writers).text = "Written by: "+ args.writers.replace("[", "").replace("]", "")
        view.findViewById<TextView>(R.id.tv_details_rated).text = "Rating: " + args.rated
        view.findViewById<TextView>(R.id.tv_details_year_and_genre).text = args.year.toString() + " | " + args.genres.replace("[", "").replace("]", "")
        view.findViewById<TextView>(R.id.tv_details_fullplot).text = args.fullPlot
        view.findViewById<TextView>(R.id.tv_details_cast).text = "Starring: "+ args.cast.replace("[", "").replace("]", "")
        view.findViewById<TextView>(R.id.tv_details_awards).text = "Awards: "+ args.awards.replace(".", "")
        view.findViewById<TextView>(R.id.tv_details_imdb).text = args.imdb
        view.findViewById<TextView>(R.id.tv_details_countries).text = "Available in: " + args.countries.replace("[", "").replace("]", "")

        if(args.imdb == "null"){
            view.findViewById<RatingBar>(R.id.bar_rating).rating = 0.0F
        }else{
            view.findViewById<RatingBar>(R.id.bar_rating).rating = args.imdb.toFloat()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_movie_details, container, false)

        return view
    }
}