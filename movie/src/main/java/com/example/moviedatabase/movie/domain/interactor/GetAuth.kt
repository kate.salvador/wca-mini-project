package com.example.moviedatabase.movie.domain.interactor

import com.example.moviedatabase.movie.domain.repository.MovieRepository

class GetAuth(private val repository: MovieRepository) {

    suspend operator fun invoke(token : String) : String {
        return repository.getAuth(token)
    }
}