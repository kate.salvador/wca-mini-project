package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Imdb (

    val rating : Double? = String.format("%.1", null).toDouble(),
    val votes : Int? = String.format("%.1", null).toInt(),
    val id : Int
)