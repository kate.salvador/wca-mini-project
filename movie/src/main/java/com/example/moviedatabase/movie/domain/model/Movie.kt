package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Movie (

    val awards : Awards,
    val imdb : Imdb,
    val tomatoes : Tomatoes = Tomatoes(Viewer(0.toDouble(),0,0), "N/A"),
    val genres : List<String>,
    val cast : List<String>,
    val countries : List<String>,
    val directors : List<String> = listOf("N/A"),
    val _id : String,
    val plot : String = "N/A",
    val runtime : Int = 0,
    val poster : String = "https://i.ibb.co/LCHZGjg/large-placeholder.png",
    val num_mflix_comments : Int = 0,
    val title : String,
    val fullplot : String = "N/A",
    val languages : List<String> = listOf("NA"),
    val released : String = "N/A",
    val writers : List<String> = listOf("NA"),
    val rated : String = "Unrated",
    val lastupdated : String,
    val year : Int,
    val type : String
)