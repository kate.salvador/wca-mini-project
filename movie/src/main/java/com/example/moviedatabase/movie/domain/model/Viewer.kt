package com.example.moviedatabase.movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Viewer (

    val rating : Double,
    val numReviews : Int,
    val meter : Int = 0
)