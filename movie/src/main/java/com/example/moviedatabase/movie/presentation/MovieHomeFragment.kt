package com.example.moviedatabase.movie.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviedatabase.movie.R
import kotlinx.android.synthetic.main.fragment_movie_home.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MovieHomeFragment : Fragment() {
    lateinit var movieController : MovieController
    private val viewModelFactory : MovieViewModelFactory by lazy { MovieViewModelFactory() }
    private val viewModel : MovieViewModel by viewModels() { viewModelFactory }

    private val rotateOpen : Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.rotate_open_anim)}
    private val rotateClose : Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.rotate_close_anim)}
    private val fromBottom : Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.from_bottom_anim)}
    private val toBottom : Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.to_bottom_anim)}

    val scope = MainScope()
    var currentPage = 1

    private var clicked = false

    var prevPage = currentPage-1
    var nextPage = currentPage+1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_home_prev.setOnClickListener {

            try {
                viewModel.loadHomeCurrentPage(prevPage.toString())
                viewModel.homeCurrentPage.observe(viewLifecycleOwner){
                    currentPage = it.toInt()
                    Toast.makeText(
                            this.requireActivity(),
                            currentPage.toString(),
                            Toast.LENGTH_SHORT
                    ).show()
                    scope.launch { loadPage(it) }

                }
            } catch (e: Exception) {
                Toast.makeText(
                        this.requireActivity(),
                        "Exception Occurred: ${e.message}",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

        btn_home_next.setOnClickListener {
            try {
                viewModel.loadHomeCurrentPage(nextPage.toString())
                viewModel.homeCurrentPage.observe(viewLifecycleOwner){
                    currentPage = it.toInt()
                    Toast.makeText(
                            this.requireActivity(),
                            currentPage.toString(),
                            Toast.LENGTH_SHORT
                    ).show()
                    scope.launch { loadPage(it) }
                }
            } catch (e: Exception) {
                Toast.makeText(
                        this.requireActivity(),
                        "Exception Occurred: ${e.message}",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

        btn_home_menu.setOnClickListener {
            onMenuButtonClicked()
        }

        btn_home_search.setOnClickListener {
            findNavController().navigate(MovieHomeFragmentDirections.actionMovieHomeFragment2ToMovieSearchFragment())
        }

        btn_home_analytics.setOnClickListener {
            findNavController().navigate(MovieHomeFragmentDirections.actionMovieHomeFragment2ToMovieAnalyticsFragment())
        }
        btn_home_filters.setOnClickListener {
            Toast.makeText(this.requireActivity(), "to filters fragment", Toast.LENGTH_SHORT).show()
        }

    }

    fun onMenuButtonClicked() {
        setVisibility(clicked)
        setAnimation(clicked)
        setClickable(clicked)
        clicked = !clicked
    }

    fun setVisibility(clicked : Boolean) {
        if(!clicked) {
            btn_home_search.visibility = View.VISIBLE
            btn_home_analytics.visibility = View.VISIBLE
            btn_home_filters.visibility = View.VISIBLE
        }else{
            btn_home_search.visibility = View.INVISIBLE
            btn_home_analytics.visibility = View.INVISIBLE
            btn_home_filters.visibility = View.INVISIBLE
        }
    }

    fun setAnimation(clicked : Boolean) {
        if(!clicked) {
            btn_home_search.startAnimation(fromBottom)
            btn_home_analytics.startAnimation(fromBottom)
            btn_home_filters.startAnimation(fromBottom)
            btn_home_menu.startAnimation(rotateOpen)
        }else{
            btn_home_search.startAnimation(toBottom)
            btn_home_analytics.startAnimation(toBottom)
            btn_home_filters.startAnimation(toBottom)
            btn_home_menu.startAnimation(rotateClose)
        }
    }

    fun setClickable(clicked : Boolean) {
        if(!clicked) {
            btn_home_search.isClickable = true
            btn_home_analytics.isClickable = true
            btn_home_filters.isClickable = true
        }else{
            btn_home_search.isClickable = false
            btn_home_analytics.isClickable = false
            btn_home_filters.isClickable = false
        }
    }

     fun loadPage(page : String) {
        initController()
        try {
            viewModel.loadMovies(page)
            viewModel.movies.observe(viewLifecycleOwner){
                movieController.setData(it)
            }
        } catch (e: Exception) {
            Toast.makeText(
                    this.requireActivity(),
                    "Exception Occurred: ${e.message}",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun initController() {

        val clickListener: (View, String) -> Unit = { view, id ->
            viewModel.loadMovieById(id)
            viewModel.movie.observe(viewLifecycleOwner) {
                val poster = it.poster
                val title = it.title
                val directors = it.directors
                val writers = it.writers
                val genres = it.genres
                val rated = it.rated
                val year = it.year
                val fullplot = it.fullplot
                val cast = it.cast
                val awards = it.awards
                val imdb = it.imdb.rating
                val countries = it.countries
                val _id = it._id

                val action = MovieHomeFragmentDirections.actionMovieHomeFragment2ToMovieDetailsFragment(
                        poster,
                        title,
                        directors.toString(),
                        writers.toString(),
                        genres.toString(),
                        rated,
                        year,
                        fullplot,
                        cast.toString(),
                        awards.text,
                        imdb.toString(),
                        countries.toString(),
                        _id
                )
                findNavController().navigate(action)
            }
        }

        movieController = MovieController(clickListener)
//      movie_recycler_view.layoutManager = LinearLayoutManager(this.requireActivity())
        movie_recycler_view.layoutManager = GridLayoutManager(this.requireActivity(), 2)
        movie_recycler_view.setControllerAndBuildModels(movieController)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        scope.launch { loadPage(currentPage.toString()) }


        return inflater.inflate(R.layout.fragment_movie_home, container, false)
    }
}