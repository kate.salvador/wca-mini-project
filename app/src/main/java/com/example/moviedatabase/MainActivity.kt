package com.example.moviedatabase


import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import com.example.moviedatabase.common.BaseActivity
import com.example.moviedatabase.movie.presentation.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(R.layout.activity_main) {

    override fun start() {
        supportActionBar?.hide()

        val animation = AnimationUtils.loadAnimation(this, R.anim.slide_animation)
        splash.startAnimation(animation)

        val intent = Intent(this, LoginActivity::class.java)
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(intent)
            finish()
        }, 2000)

    }





}