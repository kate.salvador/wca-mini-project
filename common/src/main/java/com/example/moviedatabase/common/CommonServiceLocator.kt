package com.example.moviedatabase.common

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

object CommonServiceLocator {

    // Logging Interceptor
    private val logging = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    // OkHttp Instance
    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(logging)
        .readTimeout(60, TimeUnit.MINUTES)
        .writeTimeout(60, TimeUnit.MINUTES)
        .connectTimeout(60, TimeUnit.MINUTES)
        .build()

    // Retrofit Instance
    private val contentType = "application/json".toMediaType()
    val service = Retrofit.Builder()
        .baseUrl("http://178.128.110.157:8000/")
        .client(okHttpClient)
        .addConverterFactory(Json{ignoreUnknownKeys = true}.asConverterFactory(contentType))
        .build()
}